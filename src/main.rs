use std::{
    env::args_os,
    ffi::OsString,
    fs::File,
    io::{Read, Write},
};

const PAGE_SIZE: usize = 4096;

fn main() {
    for path in args_os().skip(1) {
        if let Err(err) = cat_single(&path) {
            eprintln!("rat: {:?}: {}", path, err);
        }
    }
}

fn cat_single(path: &OsString) -> std::io::Result<()> {
    let mut file = File::open(path)?;
    let mut buf = [0u8; PAGE_SIZE];
    let mut stdout = std::io::stdout();

    loop {
        match file.read(&mut buf)? {
            0 => break,
            size => stdout.write_all(&buf[..size])?,
        }
    }
    Ok(())
}
